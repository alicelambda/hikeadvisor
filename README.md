### Members
**Nabil Zubair**  
EID: nz3222, GitLab ID: nzubair76  
Estimated Time: 8h, Actual Time: 10h  

**Alice Reuter**  
EID: crr3327, Gitlab ID: alicelambda  
Estimated Time: 15h, Actual Time: 20h  

**Joshua Trunick**  
EID: jkt737, GitLab ID: jtrunick  
Estimated Time: 15h, Actual Time: 20h  

**Long Do**  
EID:lhd379 , GitLab ID: LongDo16  
Estimated Time: 8h, Actual Time: 10h  

**Austin Aurelio**  
EID: ara3495 , GitLab ID: austinrandy0209  
Estimated Time: 9h, Actual Time: 7h  